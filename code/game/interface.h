#ifndef HG_INTERFACE_H
#define HG_INTERFACE_H

#include "user.h"
#include "action.h"
#include "../misc/collection.h"
#include "../gui/guiManager.h"
#include "../gui/guiButton.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "../gui/guiList.h"

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

extern const sf::Color gDefaultColor;
extern const sf::Color gDefaultColorPress;
extern const sf::Color gDefaultColorHover;

/**
    a gui action:
    Name
    Description
    Profit (if != 0)
    Profit per second (if != 0)
    Risk (if != 0)
    (+/-) buttons if has profit per second
**/

extern util::Collection<sf::Sound> gSounds;

namespace game {
class Interface {
public:
    Interface();
    ~Interface();

    static void create();
    static void destroy();
    static Interface* get();

    void setUser(User* user);

    void addActions(std::vector<Action> actions);
    void clearActions();

    void update(const float dt);
    void render(sf::RenderTarget* target);
    bool hasChanged();

    void moveRenderPosition(const sf::Vector2f position);
    sf::Vector2f getRenderPosition();
    void scrollUp(const float amount);
private:
    void addButton(Action* action);
    User* getUser();

    static void onPress(GuiElement* element);
    static void onHover(GuiElement* element);

    void loadTextures();

    bool mHasUserChanged;
    bool mHasButtonsChanged;

    sf::Texture mButton;
    sf::Texture mButtonHover;
    sf::Texture mButtonPress;

    User* mUser;
    std::vector<Action> mActions;
    GuiList mGuiList;
    // GuiVerticalSlider
};
}

extern game::Interface* gInterface;

#endif // HG_INTERFACE_H
