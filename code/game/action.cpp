#include "action.h"

game::Action::Action() {}
game::Action::~Action() {}

std::vector<game::Action> game::Action::loadFromFile(const std::string file) {
    if(!util::fileExists(file)) {
        throw std::runtime_error(debug::failedToLoad(file));
    }

    std::vector<game::Action> actions;

    boost::property_tree::ptree pTree;
    boost::property_tree::xml_parser::read_xml(file, pTree);

    const std::string xmlAction          = "action";
    const std::string xmlName            = "name";
    const std::string xmlDescription     = "description";
    const std::string xmlProfit          = "profit";
    const std::string xmlProfitPerSecond = "profitPerSecond";
    const std::string xmlRisk            = "risk";
    const std::string xmlUnlock          = "unlock";

    BOOST_FOREACH(boost::property_tree::ptree::value_type& v, pTree.get_child("")) {
        if(v.first == xmlAction) {
            actions.emplace_back();
            actions.back().setName           (v.second.get<std::string>(xmlName));
            actions.back().setDescription    (v.second.get<std::string>(xmlDescription));
            actions.back().setProfit         (v.second.get<double>(xmlProfit));
            actions.back().setProfitPerSecond(v.second.get<double>(xmlProfitPerSecond));
            actions.back().setRisk           (v.second.get<double>(xmlRisk));
            actions.back().setUnlock         (v.second.get<double>(xmlUnlock));
        }
    }
    return actions;
}

void game::Action::setUnlock(const double unlock) {
    mUnlock = unlock;
}
double game::Action::getUnlock() {
    return mUnlock;
}

void game::Action::setName(const std::string name) {
    mName = name;
}
std::string game::Action::getName() {
    return mName;
}

void game::Action::setDescription(const std::string description) {
    mDescription = description;
}
std::string game::Action::getDescription() {
    return mDescription;
}

void game::Action::setProfit(const double profit) {
    mProfit = profit;
}
double game::Action::getProfit() {
    return mProfit;
}

void game::Action::setProfitPerSecond(const double deltaProfit) {
    mProfitPerSecond = deltaProfit;
}
double game::Action::getProfitPerSecond() {
    return mProfitPerSecond;
}

void game::Action::setRisk(const double risk) {
    mRisk = risk;
}
double game::Action::getRisk() {
    return mRisk;
}
