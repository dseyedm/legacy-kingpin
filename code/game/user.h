#ifndef HG_USER_H
#define HG_USER_H

#include "action.h"

#include <SFML/Graphics.hpp>
#include <string>

namespace game {
class User {
public:
    User();
    ~User();

    void update();

    bool policeInvestigation();

    bool hasChanged();
    bool execute(Action* action);

    void setName(const std::string name);
    std::string getName();

    bool hasUnlocked(const double money);
    bool hasBusted();

    double getProfit();
    double getProfitPerSecond();
    double getRisk();
private:
    void setProfit(const double profit);
    void setProfitPerSecond(const double deltaProfit);
    void setRisk(const double risk);

    void policeDeduct();
    int mDeductCounter;
    bool mBusted;

    sf::Clock mTimer;

    std::string mName;

    bool mChanged;
    double mUnlock;

    double mProfit;
    double mProfitPerSecond;
    double mRisk;
};
}

#endif // HG_USER_H
