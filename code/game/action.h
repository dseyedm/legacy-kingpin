#ifndef HG_ACTION_H
#define HG_ACTION_H

#include "../misc/debug.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional/optional.hpp>

#include <string>
#include <vector>

namespace game {
class Action {
public:
    Action();
   ~Action();

    static std::vector<Action> loadFromFile(const std::string file);

    std::string getName();
    std::string getDescription();
    double getProfit();
    double getProfitPerSecond();
    double getRisk();
    double getUnlock();
private:
    void setName           (const std::string name);
    void setDescription    (const std::string description);
    void setProfit         (const double profit);
    void setProfitPerSecond(const double deltaProfit);
    void setRisk           (const double risk);
    void setUnlock         (const double unlock);

    std::string mName;
    std::string mDescription;
    double mProfit;
    double mProfitPerSecond;
    double mRisk;
    double mUnlock;
};
}

#endif // HG_ACTION_H
