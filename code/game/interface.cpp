#include "interface.h"
game::Interface* gInterface;

game::Interface::Interface() {
    mHasUserChanged = false;
    mHasButtonsChanged = false;
    mUser = nullptr;
}
game::Interface::~Interface() {
    clearActions();
    mUser = nullptr;
}
void game::Interface::create() {
    gInterface = new Interface;
    gInterface->loadTextures();
}
void game::Interface::destroy() {
    delete gInterface;
}
game::Interface* game::Interface::get() {
    debug_assert(gInterface != nullptr);
    return gInterface;
}

void game::Interface::setUser(User* user) {
    debug_assert(user != nullptr);
    mUser = user;
}
game::User* game::Interface::getUser() {
    debug_assert(mUser != nullptr);
    return mUser;
}

void game::Interface::addActions(std::vector<Action> actions) {
    for(unsigned int i=0;i<actions.size();i++) {
        mActions.push_back(actions.at(i));

        addButton(&mActions.at(i));
        if(!getUser()->hasUnlocked(mActions.at(i).getUnlock())) {
            mGuiList.setVisible(mActions.at(i).getName(), false);
        }
    }
}
void game::Interface::clearActions() {
    mActions.clear();
    mGuiList.clear();
}

void game::Interface::update(const float dt) {
    // todo: add simple scrolling
    sf::Vector2i mousePos = sf::Mouse::getPosition(gWindow);
    mGuiList.update(dt, mousePos);
    if(mHasUserChanged) { // update the available actions based on unlocks
        for(unsigned int i=0;i<mActions.size();i++) {
            if(getUser()->hasUnlocked(mActions.at(i).getUnlock()) && !mGuiList.isVisible(mActions.at(i).getName())) {
                mHasButtonsChanged = true;
                mGuiList.setVisible(mActions.at(i).getName(), true);
            }
        }
    }
}
void game::Interface::render(sf::RenderTarget* target) {
    mGuiList.render(target);
}

void game::Interface::onPress(GuiElement* element) {
    // get the element, execute that linked action with user obj
    for(unsigned int i=0;i<get()->mActions.size();i++) {
        if(get()->mActions.at(i).getName() == element->getId()) {
            get()->mHasUserChanged = true;
            if(!get()->getUser()->hasBusted()) {
                if(get()->getUser()->execute(&get()->mActions.at(i))) {
                    // click
                    gSounds.get("button_click")->play();
                    return;
                }
            }
        }
    }
    gSounds.get("action_error")->play();
}

void game::Interface::onHover(GuiElement* element) {
    gSounds.get("button_hover")->play();
}
void game::Interface::addButton(Action* action) {
    mGuiList.addButton(action);

    GuiButton* newButton = mGuiList.getButton(action->getName());

    newButton->setTexture(&mButton);
    newButton->setTextureHover(&mButtonHover);
    newButton->setTexturePress(&mButtonPress);
    newButton->setShapeOpacity(100);

    newButton->setOnHoverFunc(onHover);
    newButton->setOnPressFunc(onPress);
}
bool game::Interface::hasChanged() {
    if(mHasButtonsChanged) {
        mHasButtonsChanged = false;
        return true;
    }
    return false;
}
void game::Interface::moveRenderPosition(const sf::Vector2f position) {
    mGuiList.moveRenderPosition(position);
}
sf::Vector2f game::Interface::getRenderPosition() {
    return mGuiList.getRenderPosition();
}
void game::Interface::scrollUp(const float amount) {
    sf::Vector2f offset = sf::Vector2f(0, amount);
    mGuiList.moveRenderPosition(offset);
}

void game::Interface::loadTextures() {
    std::vector<std::string> paths;
    paths.resize(3);
    paths.at(0) = gConfigs.get("config")->get<std::string>("texture.button");
    paths.at(1) = gConfigs.get("config")->get<std::string>("texture.button_hover");
    paths.at(2) = gConfigs.get("config")->get<std::string>("texture.button_press");
    if(!mButton.loadFromFile(paths.at(0))) {
        throw std::runtime_error(debug::failedToLoad(paths.at(0)));
    }
    if(!mButtonHover.loadFromFile(paths.at(1))) {
        throw std::runtime_error(debug::failedToLoad(paths.at(1)));
    }
    if(!mButtonPress.loadFromFile(paths.at(2))) {
        throw std::runtime_error(debug::failedToLoad(paths.at(2)));
    }
}
