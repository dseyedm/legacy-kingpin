#include "user.h"

game::User::User() {
    mUnlock = 0;
    setProfit(mUnlock);
    setProfitPerSecond(0);
    setRisk(0);
    mDeductCounter = 0;
    mBusted = false;
}
game::User::~User() { }

void game::User::update() {
    const float dt = mTimer.restart().asSeconds();
    setProfit(getProfit() + getProfitPerSecond() * dt);
}

bool game::User::policeInvestigation() {
    if(getRisk() <= 0) {
        return false;
    }
    if(getRisk() >= 100) {
        policeDeduct();
        return true;
    }
    const int chance = rand()%99 + 1;
    if(chance < getRisk()) {
        policeDeduct();
        return true;
    }
    return false;
}

bool game::User::hasChanged() {
    if(mChanged) {
        mChanged = false;
        return true;
    }
    return false;
}
bool game::User::hasBusted() {
    return mBusted;
}
bool game::User::execute(Action* action) {
    if(action->getProfit() < 0) {
        if(getProfit() < -action->getProfit()) {
            return false;
        }
    }
    setProfit         (getProfit()          + action->getProfit());
    setProfitPerSecond(getProfitPerSecond() + action->getProfitPerSecond());
    setRisk           (getRisk()            + action->getRisk());
    mChanged = true;
    return true;
}

void game::User::setName(const std::string name) {
    mName = name;
}
std::string game::User::getName() {
    return mName;
}

bool game::User::hasUnlocked(const double money) {
    return (mUnlock >= money);
}
bool hasBusted();

double game::User::getProfit() {
    return mProfit;
}
double game::User::getProfitPerSecond() {
    return mProfitPerSecond;
}
double game::User::getRisk() {
    return mRisk;
}

void game::User::setProfit(const double profit) {
    if(profit > mUnlock) {
        mUnlock = profit;
    }
    mProfit = profit;
}
void game::User::setProfitPerSecond(const double deltaProfit) {
    mProfitPerSecond = deltaProfit;
}
void game::User::setRisk(const double risk) {
    mRisk = risk;
}
void game::User::policeDeduct() {
    mDeductCounter++;
    setProfitPerSecond(0);
    if(mDeductCounter == 1) {
        double oldProfit = getProfit();
        double newProfit = oldProfit * 2/3 + rand()%10 + 13;
        setProfit(newProfit);
    } else if(mDeductCounter == 2) {
        double oldProfit = getProfit();
        double newProfit = oldProfit * 1/3 + rand()%10 + 13;
        setProfit(newProfit);
    } else if(mDeductCounter >= 3) {
        setProfit(0);
        mBusted = true;
    }
}
