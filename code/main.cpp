#include "state/mainMenu.h"
#include "state/intro.h"
#include "state/stateManager.h"
#include "misc/collection.h"
#include "misc/util.h"
#include "misc/debug.h"

#include <SFML/Graphics.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>

#include <iostream>
#include <math.h>

sf::RenderWindow gWindow;
const sf::Vector2i gWindowResolution = sf::Vector2i(800, 600);
util::Collection<boost::property_tree::ptree> gConfigs;
util::Collection<sf::SoundBuffer> gSoundBuffers;
util::Collection<sf::Sound> gSounds;
util::Collection<sf::Font> gFonts;

sf::Texture gMouseTexture;
sf::Sprite gMouseSprite;

const sf::Color gDefaultColor = sf::Color(190, 190, 190);
const sf::Color gDefaultColorPress = sf::Color(125, 125, 125);
const sf::Color gDefaultColorHover = sf::Color(170, 170, 170);

void renderCursor(sf::RenderTarget* target) {
    gMouseSprite.setPosition(sf::Mouse::getPosition(gWindow).x, sf::Mouse::getPosition(gWindow).y);
    target->draw(gMouseSprite);
}

void destroy() {
    gStateMgr.destroy();
    gWindow.close();
}
void program() {
    srand(time(nullptr));
    // initialize global collections
    { // load global configs
        { // config
            const std::string configFilepath = "data/config/config.ini";
            boost::property_tree::ptree* config = gConfigs.add("config", boost::property_tree::ptree());
            boost::property_tree::ini_parser::read_ini(configFilepath, *config);
        }
        { // game
            const std::string configFilepath = "data/config/game.ini";
            boost::property_tree::ptree* config = gConfigs.add("game", boost::property_tree::ptree());
            boost::property_tree::ini_parser::read_ini(configFilepath, *config);
        }
    }
    { // load global fonts
        const std::vector<std::string> nodes {
            "default", "menu_title", "game_data"
        };
        for(unsigned int i=0;i<nodes.size();i++) {
            const std::string fontFilepath = gConfigs.get("config")->get<std::string>("font." + nodes.at(i));
            sf::Font* font = gFonts.add(nodes.at(i), sf::Font());
            if(!font->loadFromFile(fontFilepath)) {
                throw std::runtime_error(debug::failedToLoad(fontFilepath));
            }
        }
    }
    { // load global sound buffers
        const std::vector<std::string> nodes {
            "button_hover", "button_click", "action_error"
        };
        for(unsigned int i=0;i<nodes.size();i++) {
            const std::string hoverFilepath = gConfigs.get("config")->get<std::string>("sound." + nodes.at(i));
            sf::SoundBuffer* buttonHover = gSoundBuffers.add(nodes.at(i), sf::SoundBuffer());
            if(!buttonHover->loadFromFile(hoverFilepath)) {
                throw std::runtime_error(debug::failedToLoad(hoverFilepath));
            }
            sf::Sound* sound = gSounds.add(nodes.at(i), sf::Sound());
            sound->setBuffer(*gSoundBuffers.get(nodes.at(i)));
            sound->setVolume(gConfigs.get("config")->get<float>("sound." + nodes.at(i) + "_volume"));
        }
    }
    { // cursor
        const std::string cursorFilepath = gConfigs.get("config")->get<std::string>("texture.cursor");
        if(!gMouseTexture.loadFromFile(cursorFilepath)) {
            throw std::runtime_error(debug::failedToLoad(cursorFilepath));
        }
        gMouseSprite.setTexture(gMouseTexture);
    }
    // initialize window
    gWindow.create(sf::VideoMode(gWindowResolution.x,
                                 gWindowResolution.y),
                                 gConfigs.get("config")->get<std::string>("window.title"), sf::Style::Close);

    gWindow.setMouseCursorVisible(false);

    // initialize and set the state
    gMainMenuState = new MainMenu;
    gStateMgr.setState(gMainMenuState);
    while(gStateMgr.isRunning()) {
        gStateMgr.getState()->run();
    }
}

int main() {
    try {
        program();
        destroy();
    } catch(std::exception &e) {
        util::error(e.what());
        destroy();

        util::pauseProgram();
        util::exitProgram(1);
    }
    util::exitProgram();
}
