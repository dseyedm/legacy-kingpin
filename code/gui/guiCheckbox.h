#ifndef HG_GUI_CHECKBOX_H
#define HG_GUI_CHECKBOX_H

#include "guiElement.h"
#include "../misc/util.h"

typedef void (*OnCheckFunction)(GuiElement*);
typedef void (*OnUncheckFunction)(GuiElement*);

class GuiCheckbox : public GuiElement {
public:
    GuiCheckbox();
    ~GuiCheckbox() {};

    void update(const float dt, const sf::Vector2i mouse);
    void render(sf::RenderTarget* target);

    void setPosition(sf::Vector2f position);
    bool contains(sf::Vector2i point);

    void setOnCheckFunc(OnCheckFunction function);
    void setOnUncheckFunc(OnUncheckFunction function);

    void setBoxSize(const sf::Vector2f size);
    const sf::Vector2f getBoxSize() const;

    void setCheckSize(const sf::Vector2f size);
    const sf::Vector2f getCheckSize() const;

    void setBoxTexture     (sf::Texture* texture);
    void setBoxTextureHover(sf::Texture* texture);
    void setBoxTexturePress(sf::Texture* texture);

    void setCheckTexture     (sf::Texture* texture);
    void setCheckTextureHover(sf::Texture* texture);
    void setCheckTexturePress(sf::Texture* texture);

    void setShapeRender(const bool render);
    bool getShapeRender();

    void setChecked(const bool checked);
    const bool getChecked() const;

    void setActive(const bool active);
    bool getActive();

private:
    void setPress();
    void setHover();
    void setNormal();
    void executeCheckFunc();
    void executeUncheckFunc();

    bool mActive;

    OnCheckFunction mOnCheckFunc;
    OnUncheckFunction mOnUncheckFunc;

    bool mShapeRender;
    sf::Texture* mBoxTexture;
    sf::Texture* mBoxTextureHover;
    sf::Texture* mBoxTexturePress;
    sf::Texture* mCheckTexture;
    sf::Texture* mCheckTextureHover;
    sf::Texture* mCheckTexturePress;
    sf::RectangleShape mBox;
    sf::RectangleShape mCheck;
    bool mChecked;

    bool mWasContain;

    sf::Vector2i mSavedMousePosition;
    bool mWasMousePressed;
};

#endif // HG_GUI_CHECKBOX_H
