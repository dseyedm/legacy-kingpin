#include "GuiSlider.h"

GuiSlider::GuiSlider() {
    mHandleTexture      = nullptr;
    mHandleTextureHover = nullptr;
    mHandleTexturePress = nullptr;

    mBarTexture      = nullptr;

    mOnValueChangeFunc = nullptr;

    setActive(true);
    setShapeRender(true);

    mBar.setFillColor(sf::Color::White);
    mHandle.setFillColor(sf::Color::White);

    setPosition(sf::Vector2f(0, 0));
    setOrientation(Right);

    mGrabbed = false;
    mValue = 0.f;
    updateHandlePosition();
}

void GuiSlider::update(const float dt, const sf::Vector2i mouse) {
    if(mActive) {
        const bool mousePressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
        const bool isContain = contains(mouse);

        const bool firstTimeMousePressed  =  mousePressed && !mWasMousePressed;
        const bool firstTimeMouseReleased = !mousePressed &&  mWasMousePressed;
        const bool mouseNotPressed        = !mousePressed && !mWasMousePressed;

        bool executeValueChange = false;

        if(firstTimeMousePressed) {                         /// Mouse Down
            mSavedMousePosition = mouse;
            mSavedHandlePosition = mHandle.getPosition();
            if(isContain) {
                setPress();
                mGrabbed = true;
            }
        } else if(mousePressed) {                           /// Mouse Held
            if(isContain && !mWasContain && contains(mSavedMousePosition)) {
                setPress();
            }
            if(mGrabbed) {
                if(mOrientation == Right || mOrientation == Left)
                {
                    mHandle.setPosition(mSavedHandlePosition.x + mouse.x - mSavedMousePosition.x, mSavedHandlePosition.y);
                    if(mHandle.getPosition().x < mBar.getPosition().x - mHandle.getSize().x / 2) {
                        mHandle.setPosition(mBar.getPosition().x - mHandle.getSize().x / 2, mSavedHandlePosition.y);
                    } else if(mHandle.getPosition().x > mBar.getPosition().x + mBar.getSize().x - mHandle.getSize().x / 2) {
                        mHandle.setPosition(mBar.getPosition().x + mBar.getSize().x - mHandle.getSize().x / 2, mSavedHandlePosition.y);
                    }
                    if(mBar.getSize().x > 0) {
                        if(mOrientation == Right)
                            mValue = (mHandle.getPosition().x + mHandle.getSize().x / 2 - mBar.getPosition().x) / mBar.getSize().x;
                        else if(mOrientation == Left)
                            mValue = 1.f - ((mHandle.getPosition().x + mHandle.getSize().x / 2 - mBar.getPosition().x) / mBar.getSize().x);
                    }
                }
                else if(mOrientation == Up || mOrientation == Down)
                {
                    mHandle.setPosition(mSavedHandlePosition.x, mSavedHandlePosition.y + mouse.y - mSavedMousePosition.y);
                    if(mHandle.getPosition().y < mBar.getPosition().y - mHandle.getSize().y / 2) {
                        mHandle.setPosition(mSavedHandlePosition.x, mBar.getPosition().y - mHandle.getSize().y / 2);
                    } else if(mHandle.getPosition().y > mBar.getPosition().y + mBar.getSize().y - mHandle.getSize().y / 2) {
                        mHandle.setPosition(mSavedHandlePosition.x, mBar.getPosition().y + mBar.getSize().y - mHandle.getSize().y / 2);
                    }
                    if(mBar.getSize().y > 0) {
                        if(mOrientation == Up)
                            mValue = 1.f - ((mHandle.getPosition().y + mHandle.getSize().y / 2 - mBar.getPosition().y) / mBar.getSize().y);
                        else if(mOrientation == Down)
                            mValue = (mHandle.getPosition().y + mHandle.getSize().y / 2 - mBar.getPosition().y) / mBar.getSize().y;
                    }
                }
                if(mValue != mPreviousValue)
                {
                    executeValueChange = true;
                }
                mPreviousValue = mValue;
            }
        } else if(firstTimeMouseReleased) {                 /// Mouse Up
            if(isContain) {
                setHover();
            }
            mGrabbed = false;
        } else if(mouseNotPressed) {                        /// Mouse Hover
            if(isContain && !mWasContain) {
                setHover();
            }
        }
        if(!isContain) {
            setNormal();
        }
        mWasContain = isContain;
        mWasMousePressed = mousePressed;

        if(executeValueChange)
        {
            executeValueChangeFunc();
        }
    }
}

void GuiSlider::render(sf::RenderTarget* target) {
    if(mShapeRender) {
        target->draw(mBar);
        target->draw(mHandle);
    }
}

bool GuiSlider::contains(sf::Vector2i point) {
    return mHandle.getGlobalBounds().contains(point.x, point.y);
}

void GuiSlider::setOnValueChangeFunc(OnValueChangeFunction function) {
    mOnValueChangeFunc = function;
}

void GuiSlider::setPosition(sf::Vector2f position) {
    mBar.setPosition(position.x - mBar.getGlobalBounds().width / 2, position.y - mBar.getGlobalBounds().height / 2);
    updateHandlePosition();
}
const sf::Vector2f GuiSlider::getPosition() const {
    return sf::Vector2f(mBar.getPosition().x + mBar.getGlobalBounds().width / 2, mBar.getPosition().y + mBar.getGlobalBounds().height / 2);
}

void GuiSlider::setHandleSize(const sf::Vector2f size) {
    mHandle.setSize(size);
    updateHandlePosition();
}
const sf::Vector2f GuiSlider::getHandleSize() const {
    return mHandle.getSize();
}

void GuiSlider::setBarSize(const sf::Vector2f size) {
    mBar.setSize(size);
    updateHandlePosition();
}
const sf::Vector2f GuiSlider::getBarSize() const {
    return mBar.getSize();
}

void GuiSlider::setShapeRender(const bool render) {
    mShapeRender = render;
}
const bool GuiSlider::getShapeRender() const {
    return mShapeRender;
}

void GuiSlider::setValue(const float value) {
    mValue = value;
}
const float GuiSlider::getValue() const {
    return mValue;
}

void GuiSlider::setActive(const bool active) {
    mActive = active;
}
const bool GuiSlider::getActive() const {
    return mActive;
}

void GuiSlider::setOrientation(const GuiSlider::Orientation orientation)
{
    mOrientation = orientation;
}
const GuiSlider::Orientation GuiSlider::getOrientation() const
{
    return mOrientation;
}

void GuiSlider::setHandleTexture(sf::Texture* texture) {
    mHandleTexture = texture;
    mHandle.setTexture(mHandleTexture);
    mHandle.setTextureRect(sf::IntRect(1, 1, mHandleTexture->getSize().x, mHandleTexture->getSize().y));
}
void GuiSlider::setHandleTextureHover(sf::Texture* texture) {
    mHandleTextureHover = texture;
}
void GuiSlider::setHandleTexturePress(sf::Texture* texture) {
    mHandleTexturePress = texture;
}
void GuiSlider::setHandleOpacity(const float opacity) {
    mHandle.setFillColor(sf::Color(255, 255, 255, opacity));
}

void GuiSlider::setBarTexture(sf::Texture* texture) {
    mBarTexture = texture;
    mBar.setTexture(mBarTexture);
//    mBar.setTextureRect(sf::IntRect(1, 1, mBarTexture->getSize().x, mBarTexture->getSize().y));
}
void GuiSlider::setBarOpacity(const float opacity) {
    mBar.setFillColor(sf::Color(255, 255, 255, opacity));
}

void GuiSlider::updateHandlePosition() { // repositions the handle based on value
    if(mOrientation == Right)
        mHandle.setPosition(mBar.getPosition().x + (mBar.getSize().x * mValue) - mHandle.getSize().x / 2, mBar.getPosition().y + mBar.getSize().y / 2 - mHandle.getSize().y / 2);
    else if(mOrientation == Left)
        mHandle.setPosition(mBar.getPosition().x + mBar.getSize().x - (mBar.getSize().x * mValue) - mHandle.getSize().x / 2, mBar.getPosition().y + mBar.getSize().y / 2 - mHandle.getSize().y / 2);
    else if(mOrientation == Up)
        mHandle.setPosition(mBar.getPosition().x + mBar.getSize().x / 2 - mHandle.getSize().x / 2, mBar.getPosition().y + mBar.getSize().y - (mBar.getSize().y * mValue) - mHandle.getSize().y / 2);
    else //if(mOrientation == Down)
        mHandle.setPosition(mBar.getPosition().x + mBar.getSize().x / 2 - mHandle.getSize().x / 2, mBar.getPosition().y + (mBar.getSize().y * mValue) - mHandle.getSize().y / 2);
}
void GuiSlider::setPress() {
    if(mHandleTexturePress != nullptr) mHandle.setTexture(mHandleTexturePress);
    mHandle.setTextureRect(sf::IntRect(1, 1, mHandleTexturePress->getSize().x, mHandleTexturePress->getSize().y));
}
void GuiSlider::setHover() {
    if(mHandleTextureHover != nullptr) mHandle.setTexture(mHandleTextureHover);
    mHandle.setTextureRect(sf::IntRect(1, 1, mHandleTextureHover->getSize().x, mHandleTextureHover->getSize().y));
}
void GuiSlider::setNormal() {
    if(mHandleTexture != nullptr) mHandle.setTexture(mHandleTexture);
    mHandle.setTextureRect(sf::IntRect(1, 1, mHandleTexture->getSize().x, mHandleTexture->getSize().y));
}

void GuiSlider::executeValueChangeFunc() {
    if(mOnValueChangeFunc != nullptr) mOnValueChangeFunc(this);
}
