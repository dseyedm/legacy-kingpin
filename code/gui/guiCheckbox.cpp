#include "guiCheckbox.h"

GuiCheckbox::GuiCheckbox() {
    mBoxTexture      = nullptr;
    mBoxTextureHover = nullptr;
    mBoxTexturePress = nullptr;
    mCheckTexture      = nullptr;
    mCheckTextureHover = nullptr;
    mCheckTexturePress = nullptr;

    mOnCheckFunc = nullptr;
    mOnUncheckFunc = nullptr;

    setActive(true);
    setShapeRender(true);

    mBox.setFillColor(sf::Color::White);
    mCheck.setFillColor(sf::Color::White);

    setPosition(sf::Vector2f(0,0));

    mChecked = false;
}

void GuiCheckbox::update(const float dt, const sf::Vector2i mouse)
{
    if(mActive) {
        const bool mousePressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
        const bool isContain = contains(mouse);

        const bool firstTimeMousePressed  =  mousePressed && !mWasMousePressed;
        const bool firstTimeMouseReleased = !mousePressed &&  mWasMousePressed;
        const bool mouseNotPressed        = !mousePressed && !mWasMousePressed;

        bool executePress = false;

        if(firstTimeMousePressed) {                         /// Mouse Down
            mSavedMousePosition = mouse;
            if(isContain) {
                setPress();
            }
        } else if(mousePressed) {                           /// Mouse Held
            if(isContain && !mWasContain && contains(mSavedMousePosition)) {
                setPress();
            }
        } else if(firstTimeMouseReleased) {                 /// Mouse Up
            if(isContain) {
                setHover();
                if(contains(mSavedMousePosition)) {
                    mChecked = !mChecked;
                    executePress = true;
                }
            }
        } else if(mouseNotPressed) {                        /// Mouse Hover
            if(isContain && !mWasContain) {
                setHover();
            }
        }
        if(!isContain) {
            setNormal();
        }
        mWasContain = isContain;
        mWasMousePressed = mousePressed;

        if(executePress) {
            if(mChecked)
                executeCheckFunc();
            else
                executeUncheckFunc();
        }
    }
}
void GuiCheckbox::render(sf::RenderTarget* target)
{
    if(mShapeRender)
    {
        target->draw(mBox);
        if(mChecked)
        {
            target->draw(mCheck);
        }
    }
}

void GuiCheckbox::setPosition(sf::Vector2f position)
{
    mBox.setPosition(position.x - mBox.getGlobalBounds().width / 2, position.y - mBox.getGlobalBounds().height / 2);
    mCheck.setPosition(position.x - mCheck.getGlobalBounds().width / 2, position.y - mCheck.getGlobalBounds().height / 2);
}
bool GuiCheckbox::contains(sf::Vector2i point)
{
    return mBox.getGlobalBounds().contains(point.x, point.y);
}

void GuiCheckbox::setOnCheckFunc(OnCheckFunction function)
{
    mOnCheckFunc = function;
}
void GuiCheckbox::setOnUncheckFunc(OnUncheckFunction function)
{
    mOnUncheckFunc = function;
}

void GuiCheckbox::setBoxSize(const sf::Vector2f size)
{
    mBox.setSize(size);
}
const sf::Vector2f GuiCheckbox::getBoxSize() const
{
    return mBox.getSize();
}

void GuiCheckbox::setCheckSize(const sf::Vector2f size)
{
    mCheck.setSize(size);
}
const sf::Vector2f GuiCheckbox::getCheckSize() const
{
    return mCheck.getSize();
}

void GuiCheckbox::setBoxTexture(sf::Texture* texture)
{
    mBoxTexture = texture;
}
void GuiCheckbox::setBoxTextureHover(sf::Texture* texture)
{
    mBoxTextureHover = texture;
}
void GuiCheckbox::setBoxTexturePress(sf::Texture* texture)
{
    mBoxTexturePress = texture;
}

void GuiCheckbox::setCheckTexture(sf::Texture* texture)
{
    mCheckTexture = texture;
}
void GuiCheckbox::setCheckTextureHover(sf::Texture* texture)
{
    mCheckTextureHover = texture;
}
void GuiCheckbox::setCheckTexturePress(sf::Texture* texture)
{
    mCheckTexturePress = texture;
}

void GuiCheckbox::setShapeRender(const bool render)
{
    mShapeRender = render;
}
bool GuiCheckbox::getShapeRender()
{
    return mShapeRender;
}

void GuiCheckbox::setChecked(const bool checked)
{
    mChecked = checked;
}
const bool GuiCheckbox::getChecked() const
{
    return mChecked;
}

void GuiCheckbox::setActive(const bool active)
{
    mActive = active;
}
bool GuiCheckbox::getActive()
{
    return mActive;
}

void GuiCheckbox::setPress()
{
    if(mBoxTexturePress != nullptr) mBox.setTexture(mBoxTexturePress);
    if(mCheckTexturePress != nullptr) mCheck.setTexture(mCheckTexturePress);
}
void GuiCheckbox::setHover()
{
    if(mBoxTextureHover != nullptr) mBox.setTexture(mBoxTextureHover);
    if(mCheckTextureHover != nullptr) mCheck.setTexture(mCheckTextureHover);
}
void GuiCheckbox::setNormal()
{
    if(mBoxTexture != nullptr) mBox.setTexture(mBoxTexture);
    if(mCheckTexture != nullptr) mCheck.setTexture(mCheckTexture);
}

void GuiCheckbox::executeCheckFunc()
{
    if(mOnCheckFunc != nullptr) mOnCheckFunc(this);
}
void GuiCheckbox::executeUncheckFunc()
{
    if(mOnUncheckFunc != nullptr) mOnUncheckFunc(this);
}
