#include "guiManager.h"

void GuiManager::add(const std::string name, GuiElement* element) {
    debug_assert(!exists(name));
    mGuiElements.insert(std::pair<std::string, GuiElement*>(name, element));
    element->setId(name);
}
GuiElement* GuiManager::get(const std::string name) {
    if(exists(name)) {
        return mGuiElements.at(name);
    }
    util::warn(debug::noElementExists(name));
    return nullptr;
}
void GuiManager::clear() {
    mGuiElements.clear();
}

void GuiManager::update(const float dt, const sf::Vector2i mouse) {
    std::map<std::string, GuiElement*>::iterator element_iterator;
    for(element_iterator = mGuiElements.begin(); element_iterator != mGuiElements.end(); element_iterator++) {
        element_iterator->second->update(dt, mouse);
    }
}
void GuiManager::render(sf::RenderTarget* target) {
    std::map<std::string, GuiElement*>::iterator element_iterator;
    for(element_iterator = mGuiElements.begin(); element_iterator != mGuiElements.end(); element_iterator++) {
        element_iterator->second->render(target);
    }
}

bool GuiManager::exists(const std::string name) {
    return (mGuiElements.find(name) != mGuiElements.end());
}
