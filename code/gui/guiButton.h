#ifndef HG_GUI_BUTTON_H
#define HG_GUI_BUTTON_H

#include "guiElement.h"
#include "../misc/util.h"

typedef void (*OnHoverFunction)(GuiElement*);
typedef void (*OnPressFunction)(GuiElement*);

class GuiButton : public GuiElement {
public:
    GuiButton();
   ~GuiButton() {}

    void update(const float dt, const sf::Vector2i mouse);
    void render(sf::RenderTarget* target);

    void setPosition(sf::Vector2f position);
    bool contains(sf::Vector2i point);

    void setOnHoverFunc(OnHoverFunction function);
    void setOnPressFunc(OnPressFunction function);

    void setTextString(std::string text);
    void setTextSize(const float size);
    void setTextFont(sf::Font* font);

    void setTextColor(sf::Color color);
    void setTextColorHover(sf::Color color);
    void setTextColorPress(sf::Color color);

    void setTextRender(const bool render);
    bool getTextRender();

    void setShapePadding(const sf::Vector2f padding);
    void setShapeOpacity(const float opacity);
    void setShapeSize(const sf::Vector2f size);
    sf::Vector2f getShapePosition();
    void setShapePosition(const sf::Vector2f position);
    void updateShapeSize();

    void setTexture     (sf::Texture* texture);
    void setTextureHover(sf::Texture* texture);
    void setTexturePress(sf::Texture* texture);

    void setShapeRender(const bool render);
    bool getShapeRender();

    void setActive(const bool active);
    bool getActive();
private:
    void setPress();
    void setHover();
    void setNormal();
    void executeHoverFunc();
    void executePressFunc();

    bool mActive;

    OnHoverFunction mOnHoverFunc;
    OnPressFunction mOnPressFunc;

    bool mTextRender;
    sf::Color mTextNormal;
    sf::Color mTextHover;
    sf::Color mTextPress;
    sf::Text  mText;

    bool mShapeRender;
    sf::Vector2f mShapePadding;
    sf::Texture* mShapeTexture;
    sf::Texture* mShapeTextureHover;
    sf::Texture* mShapeTexturePress;
    sf::RectangleShape mShape;

    bool mWasContain;

    sf::Vector2i mSavedMousePosition;
    bool mWasMousePressed;
};

#endif // HG_GUI_BUTTON_H
