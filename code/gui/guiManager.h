#ifndef HG_GUI_MANAGER_H
#define HG_GUI_MANAGER_H

#include "guiElement.h"
#include "../misc/debug.h"

class GuiManager {
public:
    GuiManager() {}
   ~GuiManager() { clear(); }

    void add(const std::string name, GuiElement* element);
    GuiElement* get(const std::string name);
    void clear();

    bool exists(const std::string name);

    void update(const float dt, const sf::Vector2i mouse);
    void render(sf::RenderTarget* target);
private:
    std::map<std::string, GuiElement*> mGuiElements;
};

#endif // HG_GUI_MANAGER_H
