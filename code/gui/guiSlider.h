#ifndef HG_GUI_SLIDER_H
#define HG_GUI_SLIDER_H

#include "guiElement.h"
#include "../misc/util.h"

typedef void (*OnValueChangeFunction)(GuiElement*);

class GuiSlider : public GuiElement {
public:
    GuiSlider();
   ~GuiSlider() {}

    void update(const float dt, const sf::Vector2i mouse);
    void render(sf::RenderTarget* target);

    bool contains(sf::Vector2i point);

    void setOnValueChangeFunc(OnValueChangeFunction function);

    void setPosition(sf::Vector2f position);
    const sf::Vector2f getPosition() const;

    void setHandleSize(const sf::Vector2f size);
    const sf::Vector2f getHandleSize() const;

    void setBarSize(const sf::Vector2f size);
    const sf::Vector2f getBarSize() const;

    void setShapeRender(const bool render);
    const bool getShapeRender() const;

    void setValue(const float value);
    const float getValue() const;

    void setActive(const bool active);
    const bool getActive() const;

    enum Orientation
    {
        Left,
        Right,
        Up,
        Down
    };
    void setOrientation(const Orientation orientation);
    const Orientation getOrientation() const;

    void setHandleTexture     (sf::Texture* texture);
    void setHandleTextureHover(sf::Texture* texture);
    void setHandleTexturePress(sf::Texture* texture);
    void setHandleOpacity(const float opacity);

    void setBarTexture(sf::Texture* texture);
    void setBarOpacity(const float opacity);
private:
    void updateHandlePosition();
    void setPress();
    void setHover();
    void setNormal();
    void executeValueChangeFunc();

    bool mActive;
    OnValueChangeFunction mOnValueChangeFunc;
    float mValue;
    Orientation mOrientation;

    bool mShapeRender;
    sf::Texture* mHandleTexture;
    sf::Texture* mHandleTextureHover;
    sf::Texture* mHandleTexturePress;
    sf::Texture* mBarTexture;
    sf::RectangleShape mHandle;
    sf::RectangleShape mBar;

    bool mWasContain;
    sf::Vector2i mSavedMousePosition;
    sf::Vector2f mSavedHandlePosition;
    bool mGrabbed;
    bool mWasMousePressed;
    float mPreviousValue;
};

#endif // HG_GUI_SLIDER_H
