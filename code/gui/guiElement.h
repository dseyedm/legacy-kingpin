#ifndef HG_GUI_ELEMENT_H
#define HG_GUI_ELEMENT_H

#include <SFML/Graphics.hpp>

class GuiElement {
public:
    GuiElement() {}
   ~GuiElement() {}

    virtual void update(const float dt, const sf::Vector2i mouse) {}
    virtual void render(sf::RenderTarget* target) {}

    void setId(const std::string id);
    std::string getId();
private:
    std::string mId;
};

#endif // HG_GUI_ELEMENT_H
