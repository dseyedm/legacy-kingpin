#include "guiButton.h"

GuiButton::GuiButton() {
    mShapeTexture      = nullptr;
    mShapeTextureHover = nullptr;
    mShapeTexturePress = nullptr;

    mOnHoverFunc = nullptr;
    mOnPressFunc = nullptr;

    setActive(true);
    setTextRender(true);
    setShapeRender(true);

    mShape.setFillColor(sf::Color::White);

    setTextColor     (sf::Color(255, 0, 0));
    setTextColorHover(sf::Color(0, 255, 0));
    setTextColorPress(sf::Color(0, 0, 255));

    setPosition(sf::Vector2f(0, 0));
    setTextString("Default");
    setShapePadding(sf::Vector2f(0, 0));
    updateShapeSize();
}

void GuiButton::update(const float dt, const sf::Vector2i mouse) {
    if(mActive) {
        const bool mousePressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
        const bool isContain = contains(mouse);

        const bool firstTimeMousePressed  =  mousePressed && !mWasMousePressed;
        const bool firstTimeMouseReleased = !mousePressed &&  mWasMousePressed;
        const bool mouseNotPressed        = !mousePressed && !mWasMousePressed;

        bool executeHover = false;
        bool executePress = false;

        if(firstTimeMousePressed) {
            mSavedMousePosition = mouse;
            if(isContain) {
                setPress();
            }
        } else if(mousePressed) {
            if(isContain && !mWasContain && contains(mSavedMousePosition)) {
                setPress();
            }
        } else if(firstTimeMouseReleased) {
            if(isContain) {
                setHover();
                if(contains(mSavedMousePosition)) {
                    executePress = true;
                }
            }
        } else if(mouseNotPressed) {
            if(isContain && !mWasContain) {
                setHover();
                executeHover = true;
            }
        }
        if(!isContain) {
            setNormal();
        }
        mWasContain = isContain;
        mWasMousePressed = mousePressed;

        if(executePress) {
            executePressFunc();
        }
        if(executeHover) {
            executeHoverFunc();
        }
    }
}
void GuiButton::setPress() {
    mText.setColor(mTextPress);
    if(mShapeTexturePress != nullptr) mShape.setTexture(mShapeTexturePress);
}
void GuiButton::setHover() {
    mText.setColor(mTextHover);
    if(mShapeTextureHover != nullptr) mShape.setTexture(mShapeTextureHover);
}
void GuiButton::setNormal() {
    mText.setColor(mTextNormal);
    if(mShapeTexture != nullptr) mShape.setTexture(mShapeTexture);
}
void GuiButton::executeHoverFunc() {
    if(mOnHoverFunc != nullptr) mOnHoverFunc(this);
}
void GuiButton::executePressFunc() {
    if(mOnPressFunc != nullptr) mOnPressFunc(this);
}
void GuiButton::render(sf::RenderTarget* target) {
    if(mShapeRender)
        target->draw(mShape);
    if(mTextRender)
        target->draw(mText);
}

void GuiButton::setPosition(sf::Vector2f position) {
    sf::Rect<float> textBounds = mText.getGlobalBounds();
    sf::Vector2f centeredPos = sf::Vector2f(position.x - textBounds.width/2, position.y - textBounds.height/2);
    mText.setPosition((int)(centeredPos.x + 0.5), (int)(centeredPos.y + 0.5));
    mShape.setPosition(centeredPos.x - mShapePadding.x, centeredPos.y - mShapePadding.y + 10);
}

bool GuiButton::contains(sf::Vector2i point) {
    sf::Rect<float> textBounds;
    if(mShapeRender)
        textBounds = mShape.getGlobalBounds();
    else
        textBounds = mText.getGlobalBounds();

    return textBounds.contains(point.x, point.y);
}

void GuiButton::setOnHoverFunc(OnHoverFunction function) {
    mOnHoverFunc = function;
}
void GuiButton::setOnPressFunc(OnPressFunction function) {
    mOnPressFunc = function;
}

void GuiButton::setTextSize(const float size) {
    mText.setCharacterSize(size);
}
void GuiButton::setTextString(std::string text) {
    mText.setString(text);
}
void GuiButton::setTextFont(sf::Font* font) {
    mText.setFont(*font);
}

void GuiButton::setTextColor(sf::Color color) {
    mTextNormal = color;
    mText.setColor(mTextNormal);
}
void GuiButton::setTextColorHover(sf::Color color) {
    mTextHover = color;
}
void GuiButton::setTextColorPress(sf::Color color) {
    mTextPress = color;
}

void GuiButton::setTextRender(const bool render) {
    mTextRender = render;
}
bool GuiButton::getTextRender() {
    return mTextRender;
}

void GuiButton::setShapePadding(const sf::Vector2f padding) {
    mShapePadding = padding;
}
void GuiButton::setShapeOpacity(const float opacity) {
    mShape.setFillColor(sf::Color(255, 255, 255, opacity));
}
void GuiButton::setShapeSize(const sf::Vector2f size) {
    mShape.setSize(size);
}
sf::Vector2f GuiButton::getShapePosition() {
    return mShape.getPosition();
}
void GuiButton::setShapePosition(const sf::Vector2f position) {
    mShape.setPosition(position);
}
void GuiButton::updateShapeSize() { // resizes the shape based on the bounds of the text
    sf::Rect<float> bounds = mText.getGlobalBounds();
    if(bounds.width > 0 && bounds.height > 0) {
        mShape.setSize(sf::Vector2f(bounds.width + mShapePadding.x*2, bounds.height + mShapePadding.y*2));
    } else {
        mShape.setSize(sf::Vector2f(100, 100));
    }
    setPosition(mText.getPosition());
}

void GuiButton::setTexture(sf::Texture* texture) {
    mShapeTexture = texture;
    mShape.setTexture(texture);
}
void GuiButton::setTextureHover(sf::Texture* texture) {
    mShapeTextureHover = texture;
}
void GuiButton::setTexturePress(sf::Texture* texture) {
    mShapeTexturePress = texture;
}

void GuiButton::setShapeRender(const bool render) {
    mShapeRender = render;
}
bool GuiButton::getShapeRender() {
    return mShapeRender;
}

void GuiButton::setActive(const bool active) {
    mActive = active;
}
bool GuiButton::getActive() {
    return mActive;
}
