#ifndef HG_GUI_LIST_H
#define HG_GUI_LIST_H

#include "guiElement.h"
#include "guiButton.h"
#include "../misc/debug.h"
#include "../misc/collection.h"

#include "../game/action.h"

extern const sf::Vector2i gWindowResolution;
extern util::Collection<sf::Font> gFonts;

extern const sf::Color gDefaultColor;
extern const sf::Color gDefaultColorPress;
extern const sf::Color gDefaultColorHover;

class GuiList : public GuiElement {
public:
    GuiList();
    ~GuiList();

    void update(const float dt, const sf::Vector2i mouse);
    void render(sf::RenderTarget* target);

    sf::Vector2f getRenderPosition();
    void moveRenderPosition(const sf::Vector2f offset);

    void addButton(game::Action* action);
    GuiButton* getButton(const std::string id);
    void setVisible(const std::string id, const bool visible);
    bool isVisible(const std::string id);
    void clear();
private:
    bool exists(const std::string id);
    void testSizes();

    std::string getProfitCommas(const std::string profit);
    std::string getMoneyStr(const double money);

    sf::Vector2f mRenderPosition;

    std::vector<GuiButton*> mButtons;
    std::vector<GuiButton*> mVisibleButtons;

    std::vector<sf::Text> mNames;
    std::vector<sf::Text> mDescriptions;
    std::vector<sf::Text> mProfits;
    std::vector<sf::Text> mProfitPerSecond;
    std::vector<sf::Text> mRisks;
    // vertical slider
};

#endif // HG_GUI_LIST_H
