#include "guiList.h"

GuiList::GuiList() {}
GuiList::~GuiList() {
    clear();
}

void GuiList::update(const float dt, const sf::Vector2i mouse) {
    testSizes();
    for(unsigned int i=0;i<mButtons.size();i++) {
        if(mVisibleButtons.at(i) != nullptr)
            mVisibleButtons.at(i)->update(dt, mouse);
    }
}
void GuiList::render(sf::RenderTarget* target) {
    testSizes();
    for(unsigned int i=0;i<mVisibleButtons.size();i++) {
        if(mVisibleButtons.at(i) != nullptr) {
            mVisibleButtons.at(i)->render(target);
            target->draw(mNames.at(i));
            target->draw(mDescriptions.at(i));
            target->draw(mProfits.at(i));
            target->draw(mProfitPerSecond.at(i));
            target->draw(mRisks.at(i));
        }
    }
}

sf::Vector2f GuiList::getRenderPosition() {
    return mRenderPosition;
}
void GuiList::moveRenderPosition(const sf::Vector2f offset) {
    mRenderPosition.x += offset.x;
    mRenderPosition.y += offset.y;
    const int size = mButtons.size();
    for(int i=0;i<size;i++) {
        mButtons.at(i)->setShapePosition(sf::Vector2f(mButtons.at(i)->getShapePosition().x + offset.x, mButtons.at(i)->getShapePosition().y + offset.y));
        mNames.at(i).move(offset);
        mDescriptions.at(i).move(offset);
        mProfits.at(i).move(offset);
        mProfitPerSecond.at(i).move(offset);
        mRisks.at(i).move(offset);
    }
}

void GuiList::addButton(game::Action* action) {
    testSizes();
    debug_assert(!exists(action->getName()));

    const int newElement = mButtons.size();

    mButtons.push_back(new GuiButton);
    mButtons.back()->setId(action->getName());
    mVisibleButtons.push_back(mButtons.back());

    mButtons.back()->setShapeSize(sf::Vector2f(700, 80));
    sf::Vector2f newButtonPos = sf::Vector2f(0, 0 + (mVisibleButtons.size()-1)*80);
    mButtons.back()->setShapePosition(newButtonPos);

    mNames.emplace_back();
    mNames.at(newElement).setFont(*gFonts.get("default"));
    mNames.at(newElement).setString(action->getName());
    mNames.at(newElement).setColor(sf::Color(210, 210, 210));
    mNames.at(newElement).setPosition(newButtonPos.x + 5, newButtonPos.y);
    mNames.at(newElement).setCharacterSize(20);

    mDescriptions.emplace_back();
    mDescriptions.at(newElement).setFont(*gFonts.get("default"));
    mDescriptions.at(newElement).setString(action->getDescription());
    mDescriptions.at(newElement).setColor(sf::Color(160, 160, 160));
    mDescriptions.at(newElement).setPosition(newButtonPos.x + 5, newButtonPos.y + 23);
    mDescriptions.at(newElement).setCharacterSize(14);
    mDescriptions.at(newElement).setStyle(sf::Text::Italic);

    mProfits.emplace_back();
    mProfits.at(newElement).setFont(*gFonts.get("default"));
    std::string profitStr;
    if(action->getProfit() < 0) {
        mProfits.at(newElement).setColor(sf::Color(190, 190, 130));
        profitStr = "Cost: $" + getProfitCommas(getMoneyStr(-action->getProfit()));
    } else if(action->getProfit() >= 0) {
        mProfits.at(newElement).setColor(sf::Color(130, 220, 130));
        profitStr = "Profit: $" + getProfitCommas(getMoneyStr(action->getProfit()));
    }
    mProfits.at(newElement).setString(profitStr);
    mProfits.at(newElement).setPosition(newButtonPos.x + 5, newButtonPos.y + 23*2);
    mProfits.at(newElement).setCharacterSize(16);

    mProfitPerSecond.emplace_back();
    mProfitPerSecond.at(newElement).setFont(*gFonts.get("default"));
    std::string profitPerSecondStr;
    if(action->getProfitPerSecond() != 0) {
        profitPerSecondStr = " $" + getProfitCommas(getMoneyStr(action->getProfitPerSecond())) + " per second";
    }
    mProfitPerSecond.at(newElement).setString(profitPerSecondStr);
    mProfitPerSecond.at(newElement).setColor(sf::Color(130, 220, 130));
    mProfitPerSecond.at(newElement).setPosition(mProfits.at(newElement).getPosition().x + mProfits.at(newElement).getLocalBounds().width, newButtonPos.y + 23*2);
    mProfitPerSecond.at(newElement).setCharacterSize(16);

    mRisks.emplace_back();
    std::string riskStr;
    if(action->getRisk() > 0) {
        mRisks.at(newElement).setColor(sf::Color(220, 130, 130));
        riskStr = " Risk: %" + util::toString(action->getRisk());
    } else if(action->getRisk() < 0) {
        mRisks.at(newElement).setColor(sf::Color(130, 130, 220));
        riskStr = " Risk: -%" + util::toString(-action->getRisk());
    }
    mRisks.at(newElement).setFont(*gFonts.get("default"));
    mRisks.at(newElement).setString(riskStr);
    mRisks.at(newElement).setPosition(mProfitPerSecond.at(newElement).getPosition().x + mProfitPerSecond.at(newElement).getLocalBounds().width, newButtonPos.y + 23*2);
    mRisks.at(newElement).setCharacterSize(16);
}

GuiButton* GuiList::getButton(const std::string id) {
    testSizes();
    debug_assert(exists(id));
    for(unsigned int i=0;i<mButtons.size();i++) {
        if(mButtons.at(i)->getId() == id)
            return mButtons.at(i);
    }
    return nullptr;
}
void GuiList::setVisible(const std::string id, const bool visible) {
    testSizes();
    debug_assert(exists(id));
    for(unsigned int i=0;i<mButtons.size();i++) {
        if(mButtons.at(i)->getId() == id) {
            if(visible) {
                mVisibleButtons.at(i) = mButtons.at(i);
            } else {
                mVisibleButtons.at(i) = nullptr;
            }
        }
    }
}
bool GuiList::isVisible(const std::string id) {
    testSizes();
    debug_assert(exists(id));
    for(unsigned int i=0;i<mButtons.size();i++) {
        if(mButtons.at(i)->getId() == id) {
            return (mVisibleButtons.at(i) != nullptr);
        }
    }
    return false;
}
void GuiList::clear() {
    for(unsigned int i=0;i<mButtons.size();i++) {
        delete mButtons.at(i);
    }
    mButtons.clear();
    mVisibleButtons.clear();
}
bool GuiList::exists(const std::string id) {
    testSizes();
    for(unsigned int i=0;i<mButtons.size();i++) {
        if(mButtons.at(i)->getId() == id)
            return true;
    }
    return false;
}
void GuiList::testSizes() {
    debug_assert(mButtons.size() == mVisibleButtons.size());
}

std::string GuiList::getProfitCommas(const std::string profit) {
    std::string newProfit = profit;
    int digits = 0;
    for(int i=profit.size();i>0;i--) {
        if(digits >= 3) {
            digits = 0;
            newProfit.insert(i, ",");
        }
        digits++;
    }
    return newProfit;
}

std::string GuiList::getMoneyStr(const double money) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(0) << money;
    return ss.str();
}
