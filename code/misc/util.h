#ifndef HG_UTIL_H
#define HG_UTIL_H

#include <rlutil/rlutil.h>

#include <string>
#include <fstream>
#include <stdexcept>
#include <iostream>

namespace util {
    bool fileExists(const std::string filepath);
    std::string loadFile(const std::string filepath);
    template <class type>
    std::string toString(type t) {
        std::stringstream ss;
        ss << t;
        return ss.str();
    }
    void exitProgram(const int code = 0);
    enum {
        text_normal  = 0,
        text_error   = 1,
        text_warning = 2
    };
    void setColour(const int type);
    void print(const std::string text, const bool newline = true);
    void warn (const std::string text);
    void error(const std::string text);
    void pauseProgram(const std::string text = "press any key to continue...");
    float radiansToDegrees(float radians);
    float degreesToRadians(float degrees);
    std::string getDirectory(const std::string file);
}

#endif // HG_UTIL_H
