#ifndef HG_UTIL_COLLECTION
#define HG_UTIL_COLLECTION

#include "../misc/util.h"
#include "../misc/debug.h"

#include <string>
#include <vector>
#include <map>

namespace util {
template <class dataType>
class Collection {
public:
    Collection() {}
    ~Collection() { clear(); }

    dataType* add(const std::string id, const dataType object);
    dataType* get(const std::string id);
    bool exists(const std::string id);
    void remove(const std::string id);
    void clear();
private:
    std::map<std::string, dataType> mCollection;
};
}

template <class dataType>
dataType* util::Collection<dataType>::add(const std::string id, const dataType object) {
    if(!exists(id)) {
        mCollection.insert(std::pair<std::string, dataType>(id, object));
        return &mCollection.at(id);
    }
    util::error(debug::elementExists(id));
    debug_assert(0);
    return nullptr;
}
template <class dataType>
dataType* util::Collection<dataType>::get(const std::string id) {
    if(exists(id)) {
        return &mCollection.at(id);
    }
    util::warn(debug::noElementExists(id));
    return nullptr;
}
template <class dataType>
void util::Collection<dataType>::remove(const std::string id) {
    if(exists(id)) {
        mCollection.erase(id);
    } else {
        util::error(debug::noElementExists(id));
        debug_assert(0);
    }
}
template <class dataType>
void util::Collection<dataType>::clear() {
    mCollection.clear();
}
template <class dataType>
bool util::Collection<dataType>::exists(const std::string id) {
    return (mCollection.find(id) != mCollection.end());
}

#endif // HG_UTIL_COLLECTION
