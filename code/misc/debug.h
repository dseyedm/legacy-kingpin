#ifndef HG_DEBUG_H
#define HG_DEBUG_H

#include "util.h"

#include <string>

extern void destroy();

namespace debug {
    void assert_(const char *condition, const char *filename, const int line, const char *function);
    #define debug_assert(c) ((c) ? (void)0 : debug::assert_(#c, __FILE__, __LINE__, __FUNCTION__))
    std::string failedToLoad(const std::string filepath);
    std::string noElementExists(const std::string name);
    std::string elementExists(const std::string name);
}

#endif // HG_DEBUG_H
