#include "debug.h"
void debug::assert_(const char *condition, const char *filename, const int line, const char *function) {
    const std::string sCondition = condition;
    const std::string sFilename = filename;
    const std::string sLine = util::toString<int>(line);
    const std::string sFunction = function;

    destroy();  // free memory before assertion

    util::setColour(util::text_error);
    util::print("assertion [ "+sCondition+" ] failed in [ "+sFilename+" ] on line "+sLine+" in [ "+sFunction+" ]\n");
    util::setColour(util::text_normal);

    util::pauseProgram();
    util::exitProgram(-1);
}
std::string debug::failedToLoad(const std::string filepath) {
    return "failed to load '" + filepath + "'";
}
std::string debug::noElementExists(const std::string name) {
    return "no element named '" + name + "'";
}
std::string debug::elementExists(const std::string name) {
    return "element '" + name + "' already exists";
}
