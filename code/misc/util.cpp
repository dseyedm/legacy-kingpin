#include "util.h"
#include "debug.h"
bool util::fileExists(const std::string filepath) {
    std::ifstream file(filepath.c_str());
    return file;
}
std::string util::loadFile(const std::string filepath) {
    if(!fileExists(filepath))
        throw std::runtime_error("file '" + filepath + "' does not exist");

    std::ifstream filestream(filepath.c_str());
    if(!filestream)
        throw std::runtime_error("failed to load '" + filepath + "'");

    std::stringstream data;
    data << filestream.rdbuf();
    filestream.close();
    return data.str();
}
void util::exitProgram(const int code) {
    util::print("exiting...");
    exit(code);
}
void util::setColour(const int type) {
    switch(type) {
        case util::text_normal:
            rlutil::setColor(rlutil::GREY);
            break;
        case util::text_error:
            rlutil::setColor(rlutil::LIGHTRED);
            break;
        case util::text_warning:
            rlutil::setColor(rlutil::YELLOW);
            break;
        default:
            debug_assert("text type invalid" == 0);
            break;
    }
}
void util::print(const std::string text, const bool newline) {
    std::cout << text;
    if(newline)
        std::cout << "\n";
}
void util::warn(const std::string text) {
    setColour(text_warning);
    print("warning: " + text);    // todo: add logging?
    setColour(text_normal);
}
void util::error(const std::string text) {
    setColour(text_error);
    print("error: " + text);   // todo: add logging?
    setColour(text_normal);
}
void util::pauseProgram(const std::string text) {
    if(text.length() > 0) util::print(text);
    rlutil::getkey();
}
#define M_PI 3.14159265359
float util::radiansToDegrees(float radians) {
    return radians * (180.0f / (float)M_PI);
}
float util::degreesToRadians(float degrees) {
    return degrees / (180 / (float)M_PI);
}
std::string util::getDirectory(const std::string file) {
    return file.substr(0, file.find_last_of("/\\") + 1);
}
