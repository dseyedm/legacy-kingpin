#include "credits.h"
Credits* gCreditsState;
void Credits::create() {
    mLoadMainMenu = false;

    sf::Font* defaultFont = gFonts.get("default");
    sf::Texture* menuBackground = nullptr;
    { // load textures
        const std::string backgroundFilepath = gConfigs.get("config")->get<std::string>("texture.background_menu");
        menuBackground = mTextures.add("background_menu", sf::Texture());
        if(!menuBackground->loadFromFile(backgroundFilepath)) {
            throw std::runtime_error(debug::failedToLoad(backgroundFilepath));
        }
    }
    { // initialize sprites
        mBackground.setTexture(*menuBackground);
    }
    { // initialize text in credits
        mCredits.setFont(*defaultFont);
        mCredits.setColor(gDefaultColor);
        mCredits.setCharacterSize(45);
        mCredits.setString(util::loadFile(gConfigs.get("config")->get<std::string>("credits.text_path")));
        mStaringPos = sf::Vector2f(gWindowResolution.x/2 - mCredits.getGlobalBounds().width/2, gWindowResolution.y - 25);
        mCredits.setPosition(mStaringPos);
        mScrollSpeed = gConfigs.get("config")->get<float>("credits.scroll_speed");
    }
    { // back
        mGuiManager.add("bBack", &mBack);
        mBack.setTextFont(defaultFont);
        mBack.setTextString("Back");

        mBack.setTextColor(gDefaultColor);
        mBack.setTextColorHover(gDefaultColorHover);
        mBack.setTextColorPress(gDefaultColorPress);

        mBack.setShapeRender(false);
        mBack.setActive(true);

        mBack.setPosition(sf::Vector2f(35, gWindowResolution.y - 35));
        mBack.setOnHoverFunc(buttonHoverSound);
        mBack.setOnPressFunc(setStateMainMenu);
    }
    {   // load music
        const std::string file = gConfigs.get("config")->get<std::string>("sound.music");
        if(!mMusic.openFromFile(file)) {
            throw std::runtime_error(debug::failedToLoad(file));
        }
        mMusic.setLoop(true);
        mMusic.setVolume(gConfigs.get("config")->get<float>("sound.music_volume"));
    }
    mMusic.play();
}
void Credits::destroy() {
    sf::Clock timer;
    const float startVolume = mMusic.getVolume();
    while(mMusic.getVolume() >= 1) {
        mMusic.setVolume(startVolume - timer.getElapsedTime().asSeconds() * 35);
    }
    mMusic.stop();
}
void Credits::run() {
    sf::Event event;
    while(gWindow.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            gWindow.close();
            gStateMgr.exit();
        }
    }
    static sf::Clock timer;
    const  float dt = timer.restart().asSeconds();

    float textHeight = mCredits.getGlobalBounds().height;
    float textPosY   = mCredits.getPosition().y;
    const bool outOfView = (textHeight + textPosY) < 0;
    if(!outOfView)
        mCredits.move(0, (float)(-mScrollSpeed * dt));
    else
        mCredits.setPosition(mStaringPos);

    mGuiManager.update(dt, sf::Mouse::getPosition(gWindow));

    gWindow.clear();
        gWindow.draw(mBackground);
        gWindow.draw(mCredits);
        mGuiManager.render(&gWindow);
        renderCursor(&gWindow);
    gWindow.display();

    if(mLoadMainMenu) {
        mLoadMainMenu = false;
        gMainMenuState = new MainMenu;
        gStateMgr.setState(gMainMenuState);
    }
}
void Credits::setStateMainMenu(GuiElement* element) {
    buttonClickSound();
    gCreditsState->mLoadMainMenu = true;
}

void Credits::buttonHoverSound(GuiElement* element) {
    gSounds.get("button_hover")->play();
}
void Credits::buttonClickSound() {
    gSounds.get("button_click")->play();
}
