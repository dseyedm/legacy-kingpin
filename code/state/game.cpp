#include "game.h"
Game* gGameState;
void Game::create() {
    mLoading = false;
    mLoadMainMenu = false;

    sf::Font* defaultFont = gFonts.get("default");
    { // background
        const std::string file = gConfigs.get("config")->get<std::string>("texture.background_menu");
        if(!mBackgroundTexture.loadFromFile(file)) {
            throw std::runtime_error(debug::failedToLoad(file));
        }
        mBackground.setTexture(mBackgroundTexture);
    }
    { // user sidebar
        const std::string file = gConfigs.get("config")->get<std::string>("texture.side_bar");
        if(!mSidebarTexture.loadFromFile(file)) {
            throw std::runtime_error(debug::failedToLoad(file));
        }
        mSidebar.setSize(sf::Vector2f(mSidebarTexture.getSize().x, mSidebarTexture.getSize().y));
        mSidebar.setTexture(&mSidebarTexture);
        mSidebar.setFillColor(sf::Color(255, 255, 255, 100));
        mSidebar.setPosition(sf::Vector2f(0, 600-mSidebarTexture.getSize().y));
    }
    { // abilities sidebar
        const std::string file = gConfigs.get("config")->get<std::string>("texture.top_bar");
        if(!mTopbarTexture.loadFromFile(file)) {
            throw std::runtime_error(debug::failedToLoad(file));
        }
        mTopbar.setSize(sf::Vector2f(mTopbarTexture.getSize().x, mTopbarTexture.getSize().y));
        mTopbar.setTexture(&mTopbarTexture);
        mTopbar.setFillColor(sf::Color(255, 255, 255, 100));
    }
    { // exit button
        // options button
        mGuiManager.add("bBack", &mBack);
        mBack.setTextFont(defaultFont);
        mBack.setTextString("Back");

        mBack.setTextColor(gDefaultColor);
        mBack.setTextColorHover(gDefaultColorHover);
        mBack.setTextColorPress(gDefaultColorPress);

        mBack.setShapeRender(false);

        mBack.setPosition(sf::Vector2f(mSidebarTexture.getSize().x/2, gWindowResolution.y - 45));
        mBack.setOnPressFunc(Game::setStateMainMenu);
        mBack.setOnHoverFunc(Game::buttonHoverSound);
    }
    { // interface that handles all of the buttons and action callbacks
        game::Interface::create();
        game::Interface::get()->create();
        game::Interface::get()->setUser(&mPlayer);
        game::Interface::get()->addActions(game::Action::loadFromFile(gConfigs.get("game")->get<std::string>("actions.path")));
        game::Interface::get()->moveRenderPosition(sf::Vector2f(mSidebar.getSize().x, mTopbar.getSize().y));
    }
    { // counters
        { // profit
            mProfitText.setFont(*gFonts.get("game_data"));
            mProfitText.setString(getProfitStr(mPlayer.getProfit(), mPlayer.getProfitPerSecond()));
            mProfitText.setColor(sf::Color(90, 230, 90));
            mProfitText.setCharacterSize(22);
            mProfitText.setPosition(sf::Vector2f(5, 0));
        }
        { // risk
            mRiskText.setFont(*gFonts.get("game_data"));
            mRiskText.setString(getRiskStr(mPlayer.getRisk()));
            mRiskText.setColor(sf::Color(200, 120, 120));
            mRiskText.setCharacterSize(15);
            mRiskText.setPosition(sf::Vector2f(5, mProfitText.getLocalBounds().height + 3));
        }
        { // investigation
            mPoliceCounter.setFont(*gFonts.get("game_data"));
            mPoliceCounter.setString(getPoliceStr());
            mPoliceCounter.setColor(sf::Color(200, 200, 200));
            mPoliceCounter.setCharacterSize(14);
            mPoliceCounter.setPosition(sf::Vector2f(5, mRiskText.getGlobalBounds().height + mPoliceCounter.getLocalBounds().height*2));
        }
        { // notifications
            mNotification.setFont(*gFonts.get("game_data"));
            mNotification.setString("");
            mNotification.setColor(sf::Color(255, 255, 0, 255));
            mNotification.setCharacterSize(24);
            mNotification.setStyle(sf::Text::Style::Italic);
        }
    }
    mPoliceTime = 25;
}
void Game::destroy() {
    game::Interface::destroy();
}
void Game::run() {
    sf::Event event;
    while(gWindow.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            gWindow.close();
            gStateMgr.exit();
        } else if(event.type == sf::Event::MouseWheelMoved) {
            const int scrollSpeed = 15;
            if(event.mouseWheel.delta > 0) {
                if(game::Interface::get()->getRenderPosition().y < mTopbar.getSize().y) {
                    game::Interface::get()->scrollUp(scrollSpeed * event.mouseWheel.delta);
                }
            } else {
                game::Interface::get()->scrollUp(scrollSpeed * event.mouseWheel.delta);
            }
        }
    }
    static sf::Clock timer;
    const float dt = timer.restart().asSeconds();

    if(!mLoading) {
        if(mLoadMainMenu) {
            mLoading = true;
            gMainMenuState = new MainMenu;
            gStateMgr.setState(gMainMenuState);
        } else {
            mPlayer.update();
            mGuiManager.update(dt, sf::Mouse::getPosition(gWindow));
            game::Interface::get()->update(dt);

            // update counters
            mProfitText.setString(getProfitStr(mPlayer.getProfit(), mPlayer.getProfitPerSecond()));
            if(mPlayer.getRisk() >= 0) {
                mRiskText.setString(getRiskStr(mPlayer.getRisk()));
                mRiskText.setColor(sf::Color(200, 120, 120));
            } else if(mPlayer.getRisk() < 0) {
                mRiskText.setString(getRiskStr(-mPlayer.getRisk()));
                mRiskText.setColor(sf::Color(120, 120, 200));
            }
            // update the police shit
            mPoliceCounter.setString(getPoliceStr());
            if(mPoliceClock.getElapsedTime().asSeconds() >= mPoliceTime) {
                mPoliceClock.restart();
                computeJusticeSystem();
            }
            // update the notifications
            if(mNotification.getString() != "") {
                if(mNotificationClock.getElapsedTime().asSeconds() >= mNotificationTime) {
                    static float lastAlpha = 255;
                    const float alpha = lastAlpha - 255*dt;
                    if(alpha >= 0) {
                        mNotification.setColor(sf::Color(mNotification.getColor().r, mNotification.getColor().g, mNotification.getColor().b, alpha));
                        lastAlpha = alpha;
                    } else {
                        mNotificationClock.restart();
                        mNotification.setString("");
                        mNotification.setColor(sf::Color(mNotification.getColor().r, mNotification.getColor().g, mNotification.getColor().b, 255));
                    }
                }
            }

            gWindow.clear();
            gWindow.draw(mBackground);
            game::Interface::get()->render(&gWindow);
            gWindow.draw(mTopbar);
            gWindow.draw(mSidebar);
            mGuiManager.render(&gWindow);
            gWindow.draw(mProfitText);
            gWindow.draw(mRiskText);
            gWindow.draw(mPoliceCounter);
            gWindow.draw(mNotification);
            renderCursor(&gWindow);
            gWindow.display();
        }
    } else {
        gWindow.clear();
        gWindow.draw(mBackground);
        renderCursor(&gWindow);
        gWindow.display();
    }
}

void Game::computeJusticeSystem() {
    // calculate risk
    if(!mPlayer.hasBusted()) {
        double lastMoney = mPlayer.getProfit();
        if(mPlayer.policeInvestigation()) {
            gSounds.get("action_error")->play();
            if(mPlayer.hasBusted()) {
                mNotification.setColor(sf::Color(255, 0, 0));
                showNotification("Busted! Police lock you up forever", 1000);
                return;
            }
            mProfitText.setString(getProfitStr(mPlayer.getProfit(), mPlayer.getProfitPerSecond()));
            std::stringstream ss;
            ss << std::fixed << std::setprecision(0) << lastMoney - mPlayer.getProfit();
            showNotification("Bad beat: Police fine you $" + getProfitCommas(ss.str()), 8);
        }
    }
}

void Game::setStateMainMenu(GuiElement* element) {
    buttonClickSound();
    gGameState->mLoadMainMenu = true;
}
void Game::buttonHoverSound(GuiElement* element) {
    gSounds.get("button_hover")->play();
}
void Game::buttonClickSound() {
    gSounds.get("button_click")->play();
}
std::string Game::getProfitCommas(const std::string profit) {
    std::string newProfit = profit;
    int digits = 0;
    for(int i=profit.size();i>0;i--) {
        if(digits >= 3) {
            digits = 0;
            newProfit.insert(i, ",");
        }
        digits++;
    }
    return newProfit;
}
std::string Game::getProfitStr(const double profit, const double perSecond) {
    std::string str;
    {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(0) << profit;
        str = "Money: $" + getProfitCommas(ss.str());
    }
    {
        std::stringstream ss;
        if(perSecond > 0) {
            ss << std::fixed << std::setprecision(0) << perSecond;
            str += " (+$" + ss.str() + ")";
        }
    }
    return str;
}
std::string Game::getRiskStr(const double risk) {
    std::string str;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(0) << risk;
    str = "Risk of Arrest: %" + ss.str();
    return str;
}
std::string Game::getPoliceStr() {
    float timeleft = mPoliceTime - mPoliceClock.getElapsedTime().asSeconds();
    std::string str;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(0) << timeleft;
    str = "Police Investigation in: " + ss.str() + "s";
    return str;
}
void Game::showNotification(const std::string str, const float seconds) {
    mNotification.setString(str);
    mNotification.setPosition(800 - mNotification.getLocalBounds().width, 0);
    mNotificationTime = seconds;
    mNotificationClock.restart();
}
