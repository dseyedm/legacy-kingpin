#include "optionsMenu.h"
Options* gOptionsState;
void Options::create() {
    mLoading = false;
    mLoadMainMenu = false;

    sf::Font* defaultFont = gFonts.get("default");
    sf::Texture* menuBackground = nullptr;
    sf::Texture* sliderHandle = nullptr;
    sf::Texture* sliderBar = nullptr;
    { // load textures
        { // menuBackground
            const std::string backgroundFilepath = gConfigs.get("config")->get<std::string>("texture.background_menu");
            menuBackground = mTextures.add("background_menu", sf::Texture());
            if(!menuBackground->loadFromFile(backgroundFilepath)) {
                throw std::runtime_error(debug::failedToLoad(backgroundFilepath));
            }
        }
        { // sliderHandle
            const std::string sliderHandleFilepath = gConfigs.get("config")->get<std::string>("texture.slider_handle");
            sliderHandle = mTextures.add("slider_handle", sf::Texture());
            if(!sliderHandle->loadFromFile(sliderHandleFilepath)) {
                throw std::runtime_error(debug::failedToLoad(sliderHandleFilepath));
            }
        }
        { // sliderBar
            const std::string sliderBarFilepath = gConfigs.get("config")->get<std::string>("texture.slider_bar");
            sliderBar = mTextures.add("slider_bar", sf::Texture());
            if(!sliderBar->loadFromFile(sliderBarFilepath)) {
                throw std::runtime_error(debug::failedToLoad(sliderBarFilepath));
            }
        }
    }
    { // intialize sprites
        mBackgroundSprite.setTexture(*menuBackground);
    }
    { // options title
        mGuiManager.add("bOptions", &mOptions);
        mOptions.setTextFont(defaultFont);
        mOptions.setTextString("Options");
        mOptions.setTextSize(65);

        mOptions.setTextColor(gDefaultColor);

        mOptions.setShapeRender(false);
        mOptions.setActive(false);

        mOptions.setPosition(sf::Vector2f(gWindowResolution.x/2, 35));
    }
    { // initiliaze text
        mSfxText.setFont(*defaultFont);
        mSfxText.setColor(gDefaultColor);
        mSfxText.setString("Sfx Volume:");
        mSfxText.setCharacterSize(40);
        mSfxText.setPosition(sf::Vector2f(55, gWindowResolution.y/2 - 150));
    }
    {
        // sfx slider
        mGuiManager.add("bSfxSlider", &mSfxSlider);

        float value = gConfigs.get("config")->get<float>("sound.sfx_value");
        mSfxSlider.setValue(value);

        mSfxSlider.setShapeRender(true);
        mSfxSlider.setActive(true);

        mSfxSlider.setOrientation(GuiSlider::Right);
        mSfxSlider.setBarSize(sf::Vector2f(120, 20));
        mSfxSlider.setHandleSize(sf::Vector2f(35, 35));
        mSfxSlider.setPosition(sf::Vector2f(mSfxText.getPosition().x + mSfxText.getGlobalBounds().width + 85,
                                            mSfxText.getPosition().y + mSfxText.getGlobalBounds().height/2 + 16));

        mSfxSlider.setHandleTexture(sliderHandle);
        mSfxSlider.setHandleTextureHover(sliderHandle);
        mSfxSlider.setHandleTexturePress(sliderHandle);
        mSfxSlider.setHandleOpacity(255);

        mSfxSlider.setBarTexture(sliderBar);
        mSfxSlider.setBarOpacity(255);
    }
    { // back
        mGuiManager.add("bBack", &mBack);
        mBack.setTextFont(defaultFont);
        mBack.setTextString("Back");

        mBack.setTextColor(gDefaultColor);
        mBack.setTextColorHover(gDefaultColorHover);
        mBack.setTextColorPress(gDefaultColorPress);

        mBack.setShapeRender(false);
        mBack.setActive(true);

        mBack.setPosition(sf::Vector2f(35, gWindowResolution.y - 35));
        mBack.setOnHoverFunc(buttonHoverSound);
        mBack.setOnPressFunc(setStateMainMenu);
    }
}
void Options::destroy() {
    saveSfxSettings();
}
void Options::run() {
    sf::Event event;
    while(gWindow.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            gWindow.close();
            gStateMgr.exit();
        }
    }
    static sf::Clock timer;
    const  float dt = timer.restart().asSeconds();

    if(!mLoading) {
        if(mLoadMainMenu) {
            mLoading = true;
            gMainMenuState = new MainMenu;
            gStateMgr.setState(gMainMenuState);
        } else {
            mGuiManager.update(dt, sf::Mouse::getPosition(gWindow));

            gWindow.clear();
                gWindow.draw(mBackgroundSprite);
                mGuiManager.render(&gWindow);
                gWindow.draw(mSfxText);
                renderCursor(&gWindow);
            gWindow.display();
        }
    } else {
        gWindow.clear();
            gWindow.draw(mBackgroundSprite);
            renderCursor(&gWindow);
        gWindow.display();
    }
}
void Options::saveSfxSettings() {
    float sfxValue = mSfxSlider.getValue();
    // save the value
    gConfigs.get("config")->put("sound.sfx_value", sfxValue);
    // calculate and save the new volumes
    float refHover = gConfigs.get("config")->get<float>("sound.button_hover_volume_ref");
    float refClick = gConfigs.get("config")->get<float>("sound.button_click_volume_ref");
    float refActionError = gConfigs.get("config")->get<float>("sound.action_error_volume_ref");
    float newHover = refHover * sfxValue;
    float newClick = refClick * sfxValue;
    float newActionError = refActionError * sfxValue;
    gConfigs.get("config")->put("sound.button_hover_volume", newHover);
    gConfigs.get("config")->put("sound.button_click_volume", newClick);
    boost::property_tree::write_ini("data/config/config.ini", *gConfigs.get("config"));

    gSounds.get("button_hover")->setVolume(newHover);
    gSounds.get("button_click")->setVolume(newClick);
    gSounds.get("action_error")->setVolume(newActionError);
}

void Options::setStateMainMenu(GuiElement* element) {
    buttonClickSound();
    gOptionsState->mLoadMainMenu = true;
}

void Options::buttonHoverSound(GuiElement* element) {
    gOptionsState->saveSfxSettings();
    gSounds.get("button_hover")->play();
}
void Options::buttonClickSound() {
    gOptionsState->saveSfxSettings();
    gSounds.get("button_click")->play();
}
