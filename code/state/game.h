#ifndef HG_GAME_STATE_H
#define HG_GAME_STATE_H

#include "stateManager.h"
#include "intro.h"
#include "../misc/collection.h"
#include "../game/interface.h"
#include "../game/user.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <boost/property_tree/ini_parser.hpp>

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

// profit, profit per second, risk
class Game : public State {
public:
    void create();
    void destroy();
    void run();
private:
    void computeJusticeSystem();

    static void setStateMainMenu(GuiElement* element);
    static void buttonHoverSound(GuiElement* element);
    static void buttonClickSound();

    std::string getProfitCommas(const std::string profit);
    std::string getProfitStr(const double profit, const double perSecond);
    std::string getRiskStr(const double risk);
    std::string getPoliceStr();

    void showNotification(const std::string str, const float seconds);

    bool mLoading;
    bool mLoadMainMenu;

    sf::Sprite mBackground;
    sf::Texture mBackgroundTexture;

    sf::Texture mSidebarTexture;
    sf::RectangleShape mSidebar;

    sf::Texture mTopbarTexture;
    sf::RectangleShape mTopbar;

    sf::Text mProfitText;
    sf::Text mRiskText;
    sf::Text mPoliceCounter;
    sf::Clock mPoliceClock;
    float mPoliceTime;

    sf::Text mNotification;
    float mNotificationTime;
    sf::Clock mNotificationClock;

    GuiButton mBack;
    GuiManager mGuiManager;

    game::User mPlayer;
};

extern Game* gGameState;

#endif // HG_GAME_STATE_H
