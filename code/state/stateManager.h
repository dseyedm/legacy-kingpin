#ifndef HG_STATE_MANAGER_H
#define HG_STATE_MANAGER_H

#include "../misc/debug.h"

#include <SFML/Graphics.hpp>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>

extern sf::RenderWindow gWindow;

class State {
public:
    virtual void create()  {}
    virtual void destroy() {}
    virtual void run() {
        sf::Event event;
        while(gWindow.pollEvent(event)) { }

        gWindow.clear();
        gWindow.display();
    }
};

class StateManager {
public:
    StateManager();
    ~StateManager();

    void exit();
    bool isRunning();
    void destroy();
    void setState(State* state);
    State* getState();
private:
    void createState(boost::atomic<bool>* done, State* state);
    void destroyState(boost::atomic<bool>* done, State* state);

    static void threadCreateState(boost::exception_ptr& error, StateManager* instance, boost::atomic<bool>* done, State* state);
    static void threadDestroyState(boost::exception_ptr& error, StateManager* instance, boost::atomic<bool>* done, State* state);

    bool   mRunning;
    State* mStateDefault;
    State* mState;
};

extern StateManager gStateMgr;

#endif // HG_STATE_MANAGER_H
