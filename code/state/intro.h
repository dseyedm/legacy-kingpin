#ifndef HG_INTRO_H
#define HG_INTRO_H

#include "stateManager.h"
#include "mainMenu.h"
#include "../misc/collection.h"
#include "../gui/guiButton.h"
#include "../gui/guiManager.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <vector>

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

class Intro : public State {
public:
    void create();
    void destroy();
    void run();
private:
    bool mLoading;
    bool mChangeState;

    float mSlideSpeed;
    unsigned int mCurrentSlide;
    std::vector<std::string> mSlides;

    sf::Vector2f mPosition;
    sf::Text mIntroText;
};

extern Intro* gIntroState;

#endif // HG_INTRO_H
