#include "intro.h"
Intro* gIntroState;
void Intro::create() {
    mLoading = false;
    mChangeState = false;

    mIntroText.setFont(*gFonts.get("default"));
    const std::string fileString = util::loadFile(gConfigs.get("config")->get<std::string>("intro.text_path"));
    std::string slide;
    for(unsigned int i = 0; i < fileString.size(); i++) {
        slide.push_back(fileString.at(i));
        if(fileString.at(i) == '\n' || i == fileString.size()-1) {
            mSlides.push_back(slide);
            slide.clear();
        }
    }

    mSlideSpeed = gConfigs.get("config")->get<float>("intro.slide_speed");
    mCurrentSlide = 0;
    mIntroText.setString(mSlides.at(mCurrentSlide));
    mIntroText.setCharacterSize(28);
    mPosition = sf::Vector2f(gWindowResolution.x/2, 55);
    mIntroText.setPosition((int)(mPosition.x - mIntroText.getGlobalBounds().width/2),
                           (int)(mPosition.y));
    mIntroText.setColor(sf::Color(200, 200, 200));
    mIntroText.setStyle(sf::Text::Style::Italic);
}
void Intro::destroy() { }
void Intro::run() {
    sf::Event event;
    while(gWindow.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            gWindow.close();
            gStateMgr.exit();
        }
    }
    static sf::Clock timer;
    const  float currentTime = timer.getElapsedTime().asSeconds();
//    static float lastTime = 0;
//    const  float dt = float(currentTime - lastTime);
//    lastTime = currentTime;

    if(!mLoading) {
        if(currentTime > mSlideSpeed) {
            timer.restart();
            mCurrentSlide++;
            if(mCurrentSlide < mSlides.size()) {
                mIntroText.setString(mSlides.at(mCurrentSlide));
                mIntroText.setPosition((int)(mPosition.x - mIntroText.getGlobalBounds().width/2),
                                       (int)(mPosition.y));
            } else {
                mChangeState = true;
                mLoading = true;
            }
        }

        gWindow.clear();
            gWindow.draw(mIntroText);
            renderCursor(&gWindow);
        gWindow.display();
    } else {
        if(mChangeState) {
            mChangeState = false;
            gMainMenuState = new MainMenu;
            gStateMgr.setState(gMainMenuState);
        }
        gWindow.clear();
        renderCursor(&gWindow);
        gWindow.display();
    }
}
