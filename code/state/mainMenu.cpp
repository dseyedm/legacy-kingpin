#include "mainMenu.h"
MainMenu* gMainMenuState;
void MainMenu::create() {
    mLoading = false;
    mLoadGameState = false;
    mLoadOptionsState = false;
    mLoadCreditsState = false;

    sf::Font* defaultFont = gFonts.get("default");
    sf::Font* menuTitleFont = gFonts.get("menu_title");

    sf::Texture* menuBackground = nullptr;
    {
        // load textures
        { // menuBackground
            const std::string backgroundFilepath = gConfigs.get("config")->get<std::string>("texture.background_menu");
            menuBackground = mTextures.add("background_menu", sf::Texture());
            if(!menuBackground->loadFromFile(backgroundFilepath)) {
                throw std::runtime_error(debug::failedToLoad(backgroundFilepath));
            }
        }
    }

    mBackgroundSprite.setTexture(*menuBackground);

    const int posOffset = 50;
    const float defaultSize = 50;
    const int startY = gWindowResolution.y/2 - defaultSize/2 + 30;

    mLoadingText.setFont(*defaultFont);
    mLoadingText.setString(" Loading..");
    mLoadingText.setColor(gDefaultColor);

    {
        // start button
        mGuiManager.add("bAmericanDream", &mAmericanDream);
        mAmericanDream.setTextFont(menuTitleFont);
        mAmericanDream.setTextString("Kingpin");
        mAmericanDream.setTextSize(defaultSize + 35);

        mAmericanDream.setTextColor(sf::Color(230, 230, 230));

        mAmericanDream.setShapeRender(false);
        mAmericanDream.setActive(false);

        mAmericanDream.setPosition(sf::Vector2f(gWindowResolution.x/2, startY - posOffset - 65));
    }

    {
        // start button
        mGuiManager.add("bStart", &mStartButton);
        mStartButton.setTextFont(defaultFont);
        mStartButton.setTextString("Start");
        mStartButton.setTextSize(defaultSize);

        mStartButton.setTextColor(gDefaultColor);
        mStartButton.setTextColorHover(gDefaultColorHover);
        mStartButton.setTextColorPress(gDefaultColorPress);

        mStartButton.setShapeRender(false);

        mStartButton.setPosition(sf::Vector2f(gWindowResolution.x/2, startY));
        mStartButton.setOnPressFunc(MainMenu::setStateGame);
        mStartButton.setOnHoverFunc(MainMenu::buttonHoverSound);
    }

    {
        // options button
        mGuiManager.add("bOptions", &mOptionsButton);
        mOptionsButton.setTextFont(defaultFont);
        mOptionsButton.setTextString("Options");
        mOptionsButton.setTextSize(defaultSize);

        mOptionsButton.setTextColor(gDefaultColor);
        mOptionsButton.setTextColorHover(gDefaultColorHover);
        mOptionsButton.setTextColorPress(gDefaultColorPress);

        mOptionsButton.setShapeRender(false);

        mOptionsButton.setPosition(sf::Vector2f(gWindowResolution.x/2, startY + posOffset * 1));
        mOptionsButton.setOnPressFunc(MainMenu::setStateOptions);
        mOptionsButton.setOnHoverFunc(MainMenu::buttonHoverSound);
    }

    {
        // credits button
        mGuiManager.add("bCredits", &mCreditsButton);
        mCreditsButton.setTextFont(defaultFont);
        mCreditsButton.setTextString("Credits");
        mCreditsButton.setTextSize(defaultSize);

        mCreditsButton.setTextColor(gDefaultColor);
        mCreditsButton.setTextColorHover(gDefaultColorHover);
        mCreditsButton.setTextColorPress(gDefaultColorPress);

        mCreditsButton.setShapeRender(false);

        mCreditsButton.setPosition(sf::Vector2f(gWindowResolution.x/2, startY + posOffset * 2));
        mCreditsButton.setOnPressFunc(MainMenu::setStateCredits);
        mCreditsButton.setOnHoverFunc(MainMenu::buttonHoverSound);
    }

    {
        // exit button
        mGuiManager.add("bExit", &mExitButton);
        mExitButton.setTextFont(defaultFont);
        mExitButton.setTextString("Exit");
        mExitButton.setTextSize(defaultSize);

        mExitButton.setTextColor(gDefaultColor);
        mExitButton.setTextColorHover(gDefaultColorHover);
        mExitButton.setTextColorPress(gDefaultColorPress);

        mExitButton.setShapeRender(false);

        mExitButton.setPosition(sf::Vector2f(gWindowResolution.x/2, startY + posOffset * 3));
        mExitButton.setOnPressFunc(MainMenu::exit);
        mExitButton.setOnHoverFunc(MainMenu::buttonHoverSound);
    }
}
void MainMenu::destroy() {
    ;
}
void MainMenu::run() {
    sf::Event event;
    while(gWindow.pollEvent(event)) {
        if(event.type == sf::Event::Closed) {
            gWindow.close();
            gStateMgr.exit();
        }
    }
    static sf::Clock timer;
    const  float dt = timer.restart().asSeconds();

    if(!mLoading) {
        if(mLoadGameState) {
            mLoading = true;
            gGameState = new Game;
            gStateMgr.setState(gGameState);
        } else if(mLoadOptionsState) {
            mLoading = true;
            gOptionsState = new Options;
            gStateMgr.setState(gOptionsState);
        } else if(mLoadCreditsState) {
            mLoading = true;
            gCreditsState = new Credits;
            gStateMgr.setState(gCreditsState);
        } else {
            mGuiManager.update(dt, sf::Mouse::getPosition(gWindow));

            gWindow.clear();
                gWindow.draw(mBackgroundSprite);
                mGuiManager.render(&gWindow); // render the gui
                renderCursor(&gWindow);
            gWindow.display();
        }
    } else {
        if(timer.getElapsedTime().asSeconds() > 0.2) {
            timer.restart();
            std::string newString = mLoadingText.getString();
            newString.push_back('.');
            mLoadingText.setString(newString);
        }

        gWindow.clear();
        gWindow.draw(mBackgroundSprite);
        gWindow.draw(mLoadingText);
        renderCursor(&gWindow);
        gWindow.display();
    }
}

void MainMenu::exit(GuiElement* element) {
    buttonClickSound();
    gStateMgr.exit();
}
void MainMenu::setStateOptions(GuiElement* element) {
    buttonClickSound();
    gMainMenuState->mLoadOptionsState = true;
}
void MainMenu::setStateCredits(GuiElement* element) {
    buttonClickSound();
    gMainMenuState->mLoadCreditsState = true;
}
void MainMenu::setStateGame(GuiElement* element) {
    buttonClickSound();
    gMainMenuState->mLoadGameState = true;

}
void MainMenu::buttonHoverSound(GuiElement* element) {
    gSounds.get("button_hover")->play();
}
void MainMenu::buttonClickSound() {
    gSounds.get("button_click")->play();
}
