#ifndef HG_OPTIONS_MENU_H
#define HG_OPTIONS_MENU_H

#include "stateManager.h"
#include "mainMenu.h"
#include "../gui/guiButton.h"
#include "../gui/guiSlider.h"
#include "../gui/guiManager.h"
#include "../misc/collection.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <boost/property_tree/ini_parser.hpp>

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

// sfx and music volume
class Options : public State {
public:
    void create();
    void destroy();
    void run();
private:
    void saveSfxSettings();

    bool mLoading;
    bool mLoadMainMenu;

    sf::Text mSfxText;

    GuiButton  mOptions;
    GuiButton  mBack;
    GuiSlider  mSfxSlider;
    GuiManager mGuiManager;

    static void setStateMainMenu(GuiElement* element);

    static void buttonHoverSound(GuiElement* element);
    static void buttonClickSound();

    sf::Sprite mBackgroundSprite;

    util::Collection<sf::Texture> mTextures;
};

extern Options* gOptionsState;

#endif // HG_OPTIONS_MENU_H
