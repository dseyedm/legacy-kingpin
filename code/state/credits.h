#ifndef HG_CREDITS_H
#define HG_CREDITS_H

#include "stateManager.h"
#include "mainMenu.h"
#include "../misc/collection.h"
#include "../gui/guiButton.h"
#include "../gui/guiManager.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <boost/property_tree/ini_parser.hpp>

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

extern void renderCursor(sf::RenderTarget* target);

class Credits : public State {
public:
    void create();
    void destroy();
    void run();
private:
    sf::Sprite mBackground;

    sf::Vector2f mStaringPos;
    float mScrollSpeed;
    sf::Text mCredits;

    bool mLoadMainMenu;
    GuiButton  mBack;
    GuiManager mGuiManager;

    sf::Music mMusic;

    static void setStateMainMenu(GuiElement* element);

    static void buttonHoverSound(GuiElement* element);
    static void buttonClickSound();

    util::Collection<sf::Texture> mTextures;
};

extern Credits* gCreditsState;

#endif // HG_CREDITS_H
