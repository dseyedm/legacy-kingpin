#include "stateManager.h"
StateManager gStateMgr;

StateManager::StateManager() {
    mStateDefault = new State;
    mState = mStateDefault;
    getState()->create();
    mRunning = true;
}
StateManager::~StateManager() {
    ;   // assume that StateManager::destroy has been called
}

void StateManager::exit() {
    mRunning = false;
}

bool StateManager::isRunning() {
    return mRunning;
}

void StateManager::destroy() {
    if(mState != nullptr) {
        getState()->destroy();
    }
    delete mState;
    mState = nullptr;
}

void StateManager::createState(boost::atomic<bool>* done, State* state) {
    state->create();
    done->store(true);
}
void StateManager::threadCreateState(boost::exception_ptr& error, StateManager* instance, boost::atomic<bool>* done, State* state) {
    try {
        instance->createState(done, state);
        error = boost::exception_ptr();
    } catch(std::exception& e) {
        error = boost::current_exception();
    }
}

void StateManager::destroyState(boost::atomic<bool>* done, State* state) {
    state->destroy();
    delete state;
    state = nullptr;
    done->store(true);
}
void StateManager::threadDestroyState(boost::exception_ptr& error, StateManager* instance, boost::atomic<bool>* done, State* state) {
    try {
        instance->destroyState(done, state);
        error = boost::exception_ptr();
    } catch(std::exception& e) {
        error = boost::current_exception();
    }
}
void StateManager::setState(State* state) {
    debug_assert(state != nullptr);
    debug_assert(mState != nullptr);

    { // create the new state while running the last
        boost::atomic<bool> done(false);
        boost::exception_ptr error;
        boost::thread thread(boost::bind(threadCreateState, boost::ref(error), this, &done, state));
        { // while thread is running
            sf::Context context;
            while(!done.load() && !error) {
                mState->run();
            }
        }
        thread.join();
        if(error) {
            boost::rethrow_exception(error);
        }
    }

    { // delete the first state while running the new one
        boost::atomic<bool> done(false);
        boost::exception_ptr error;
        boost::thread thread(boost::bind(threadDestroyState, boost::ref(error), this, &done, mState));
        { // while thread is running
            sf::Context context;
            while(!done.load() && !error) {
                state->run();
            }
        }
        thread.join();
        if(error) {
            boost::rethrow_exception(error);
        }
    }

    // replace the old, deleted state with the new one
    mState = state;
}
State* StateManager::getState() {
    debug_assert(mState != nullptr); // check if state is invalid
    return mState;
}
