#ifndef HG_STATE_MAIN_MENU_H
#define HG_STATE_MAIN_MENU_H

#include "optionsMenu.h"
#include "game.h"
#include "intro.h"
#include "credits.h"
#include "stateManager.h"

#include "../game/action.h"
#include "../game/interface.h"

#include "../gui/guiManager.h"
#include "../gui/guiSlider.h"
#include "../gui/guiButton.h"
#include "../misc/util.h"
#include "../misc/collection.h"

#include <SFML/Audio.hpp>
#include <boost/property_tree/ini_parser.hpp>

extern sf::RenderWindow gWindow;
extern const sf::Vector2i gWindowResolution;

extern util::Collection<boost::property_tree::ptree> gConfigs;
extern util::Collection<sf::SoundBuffer> gSoundBuffers;
extern util::Collection<sf::Font> gFonts;

class MainMenu : public State {
public:
    void create();
    void destroy();
    void run();
private:
    bool mLoading;
    bool mLoadGameState;
    bool mLoadOptionsState;
    bool mLoadCreditsState;

    sf::Text mLoadingText;
    sf::Sprite mBackgroundSprite;

    static void exit(GuiElement* element);
    static void setStateOptions(GuiElement* element);
    static void setStateCredits(GuiElement* element);
    static void setStateGame(GuiElement* element);

    static void buttonHoverSound(GuiElement* element);
    static void buttonClickSound();

    GuiButton mAmericanDream;
    GuiButton mStartButton;
    GuiButton mOptionsButton;
    GuiButton mCreditsButton;
    GuiButton mExitButton;

    GuiManager mGuiManager;
    util::Collection<sf::Texture> mTextures;
};

extern MainMenu* gMainMenuState;

#endif // HG_STATE_MAIN_MENU_H
